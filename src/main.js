const { createServer } = require('./server.js');
const path = require('path');
const dir = path.join(process.cwd(), './dist');

/*代理本地文件*/
const server = createServer({ dir, port: 3888, base: '/vite-project/', single: true });

/*远程代理*/
// const server = createServer({ dir: 'http://peryl.gitee.io/', port: 3888, single: true, cors: true });

server.start();
