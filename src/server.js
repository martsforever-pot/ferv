"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pathJoin = exports.createServer = void 0;
var http = require('http');
var path = require('path');
var mime = require('mime');
var chalk = require('chalk');
var url = require('url');
var _a = require('fs'), fs = _a.promises, createReadStream = _a.createReadStream;
var _importDynamic = new Function('modulePath', 'return import(modulePath)');
function fetch() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return __awaiter(this, void 0, void 0, function () {
        var fetch;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, _importDynamic('node-fetch')];
                case 1:
                    fetch = (_a.sent()).default;
                    // @ts-ignore
                    return [2 /*return*/, fetch.apply(void 0, args)];
            }
        });
    });
}
function createServer(config) {
    var isProxy = /https?/.test(config.dir);
    if (!!config.base) {
        config.base = pathJoin("/".concat(config.base, "/"));
    }
    function logDebug(val) { if (config.debug) {
        console.log(val);
    } }
    function logError(val) { if (config.error !== false) {
        console.error(val);
    } }
    /**
     * 响应失败
     * @author  韦胜健
     * @date    2023/3/7 16:39
     */
    function sendError(err, res) {
        logError(err);
        res.statusCode = 404;
        res.statusMessage = 'Not Found';
        res.end();
    }
    /**
     * 响应成功，返回目标文件
     * @author  韦胜健
     * @date    2023/3/7 16:40
     */
    function sendFile(path, req, res, stat) {
        res.setHeader('Content-Type', mime.getType(path) + ';charset=utf-8');
        if (config.cors) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Headers', '*');
            res.setHeader('Access-Control-Allow-Methods', '*');
        }
        createReadStream(path).pipe(res);
    }
    /**
     * 返回首页文件
     * @author  韦胜健
     * @date    2023/3/7 16:47
     */
    function sendIndex(req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var indexPath, indexStat;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        indexPath = path.join(config.dir, 'index.html');
                        return [4 /*yield*/, fs.stat(indexPath)];
                    case 1:
                        indexStat = _a.sent();
                        if (indexStat.isFile()) {
                            sendFile(indexPath, req, res, indexStat);
                        }
                        else {
                            throw new Error('index.html missing!');
                        }
                        return [2 /*return*/];
                }
            });
        });
    }
    /**
     * 下载远程文件然后返回
     * @author  韦胜健
     * @date    2024.3.2 22:59
     */
    function sendRemote(url, req, res) {
        var fileType = mime.getType(url) || 'text/html';
        // console.log({ url, fileType });
        res.setHeader('Content-Type', fileType + ';charset=utf-8');
        if (config.cors) {
            res.setHeader('Access-Control-Allow-Origin', '*');
        }
        fetch(url).then(function (r) {
            r.body.pipe(res);
        });
    }
    /**
     * 处理请求
     * @author  韦胜健
     * @date    2023/3/7 16:40
     */
    function handleRequest(req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var pathname, url_1, absPath, stat, indexPath, indexStat, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pathname = url.parse(req.url || '').pathname;
                        if (!pathname) {
                            pathname = '/';
                        }
                        pathname = pathJoin("/".concat(pathname));
                        if (!isProxy) return [3 /*break*/, 1];
                        url_1 = pathJoin(config.dir, pathname);
                        sendRemote(url_1, req, res);
                        return [3 /*break*/, 15];
                    case 1:
                        if (!!config.base && !!config.base.length && pathname.startsWith(config.base)) {
                            pathname = pathname.slice(config.base.length);
                        }
                        pathname = decodeURIComponent(pathname);
                        absPath = path.join(config.dir, pathname);
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 11, , 15]);
                        return [4 /*yield*/, fs.stat(absPath)];
                    case 3:
                        stat = _a.sent();
                        if (!stat.isDirectory()) return [3 /*break*/, 9];
                        indexPath = path.join(absPath, 'index.html');
                        return [4 /*yield*/, fs.stat(indexPath)];
                    case 4:
                        indexStat = _a.sent();
                        if (!indexStat.isFile()) return [3 /*break*/, 5];
                        logDebug({ pathname: pathname, indexPath: indexPath, });
                        sendFile(indexPath, req, res, indexStat);
                        return [3 /*break*/, 8];
                    case 5:
                        if (!config.single) return [3 /*break*/, 7];
                        logDebug({ pathname: pathname, msg: 'no match file, return index path', });
                        return [4 /*yield*/, sendIndex(req, res)];
                    case 6:
                        _a.sent();
                        return [3 /*break*/, 8];
                    case 7: throw new Error('no file match path:' + pathname);
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        logDebug({ pathname: pathname, });
                        sendFile(absPath, req, res, stat);
                        _a.label = 10;
                    case 10: return [3 /*break*/, 15];
                    case 11:
                        e_1 = _a.sent();
                        if (!config.single) return [3 /*break*/, 13];
                        logDebug({ pathname: pathname, msg: 'no match file, return index path', });
                        return [4 /*yield*/, sendIndex(req, res)];
                    case 12:
                        _a.sent();
                        return [3 /*break*/, 14];
                    case 13:
                        sendError({ error: e_1, pathname: pathname }, res);
                        _a.label = 14;
                    case 14: return [3 /*break*/, 15];
                    case 15: return [2 /*return*/];
                }
            });
        });
    }
    function start() {
        return http.createServer(handleRequest).listen(config.port, function () {
            console.log("\n".concat(chalk.yellow("Starting up http-server, serving:"), "\n").concat(isProxy ? "\u4EE3\u7406\u8FDC\u7A0B\u5730\u5740".concat(config.dir) : "\u4EE3\u7406\u672C\u5730\u6587\u4EF6".concat(config.dir), " :\n").concat(pathJoin("http://127.0.0.1:".concat(chalk.green(config.port)), config.base), "\nHit Ctrl-C to stop the Server\n            ").trim());
        });
    }
    return { start: start };
}
exports.createServer = createServer;
function pathJoin() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var val = args.filter(function (i) {
        if (i == null) {
            return false;
        }
        if (i.trim() == '/') {
            return false;
        }
        return true;
    }).reduce(function (prev, item) {
        if (!prev) {
            return item;
        }
        return prev + '/' + item;
    }, '');
    var prependStringList = ['http://', 'https://'];
    var prependString = prependStringList.find(function (i) { return val.indexOf(i) === 0; });
    var ret;
    if (!prependString) {
        /*非http.https开头*/
        ret = val.replace(/\/{2,}/g, '/');
    }
    else {
        var leftString = val.slice(prependString.length);
        // console.log({ prependString, leftString });
        ret = prependString + leftString.replace(/\/{2,}/g, '/');
    }
    if (ret === '/') {
        return '';
    }
    return ret;
}
exports.pathJoin = pathJoin;
