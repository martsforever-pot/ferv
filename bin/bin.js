#! /usr/bin/env node

const { createServer } = require('../src/server.js');
const packageJson = require('../package.json');
const program = require('commander');

const args = { dir: null };

program.name('servef')
  .description('代理静态资源的一个脚手架')
  .usage('<optoins>')
  .version(packageJson.version, '-V, --version', '打印版本')
  .arguments('<dir>')
  .action(function (dir) {args.dir = dir;});

const configs = {
  '-p, --port <port>': '端口',
  '-d, --dir <dir>': '代理文件夹路径或者代理远程路径',
  '-s, --single': '单页面应用，当请求url没有匹配内容时返回/index.html',
  '-d, --debug <debug>': '打印所有请求信息',
  '-ne, --error': '报错时不打印请求错误信息',
  '-c, --cors': '响应头添加Access-Control-Allow-Origin:*用来跨域',
  '-b, --base <base>': '公共资源路径',
};

Object.entries(configs).forEach(([key, value]) => program.option(key, value));

/*这个必须得在program调用option之后调用，否则无法识别，命令行参数*/
program.parse(process.argv);
const options = program.opts();
let defaultConfig = { port: 12321, dir: process.cwd(), single: true, ...args, ...options, };
if (!!defaultConfig.error) {defaultConfig.error = false;}

console.log('proxy config:' + JSON.stringify(defaultConfig));

let server = createServer(defaultConfig);

server.start();
