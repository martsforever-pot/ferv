import {IncomingMessage, ServerResponse} from 'http';
import {Stats} from "node:fs";

const http = require('http');
const path = require('path');
const mime = require('mime');
const chalk = require('chalk');
const url = require('url');

const { promises: fs, createReadStream } = require('fs');

const _importDynamic = new Function('modulePath', 'return import(modulePath)');

async function fetch(...args: any[]) {
  const { default: fetch } = await _importDynamic('node-fetch');
  // @ts-ignore
  return fetch(...args);
}

export interface iServerConfig {
  port: number,                     // 监听的端口
  dir: string,                      // 代理的路径
  single?: boolean,                 // 找不到对应资源时返回index.html
  debug?: boolean,                  // 显示普通请求日志
  error?: boolean,                  // 显示错误请求日志
  cors?: boolean,                   // 设置跨域头
  base?: string,                    // 公共资源路径
}

export function createServer(config: iServerConfig) {

  const isProxy = /https?/.test(config.dir);

  if (!!config.base) {config.base = pathJoin(`/${config.base}/`);}

  function logDebug(val: any) {if (config.debug) {console.log(val);}}

  function logError(val: any) {if (config.error !== false) {console.error(val);}}

  /**
   * 响应失败
   * @author  韦胜健
   * @date    2023/3/7 16:39
   */
  function sendError(err: unknown, res: ServerResponse) {
    logError(err);
    res.statusCode = 404;
    res.statusMessage = 'Not Found';
    res.end();
  }

  /**
   * 响应成功，返回目标文件
   * @author  韦胜健
   * @date    2023/3/7 16:40
   */
  function sendFile(path: string, req: IncomingMessage, res: ServerResponse, stat: Stats) {
    res.setHeader('Content-Type', mime.getType(path) + ';charset=utf-8');
    if (config.cors) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Headers', '*');
      res.setHeader('Access-Control-Allow-Methods', '*');
    }
    createReadStream(path).pipe(res);
  }

  /**
   * 返回首页文件
   * @author  韦胜健
   * @date    2023/3/7 16:47
   */
  async function sendIndex(req: IncomingMessage, res: ServerResponse) {
    const indexPath = path.join(config.dir, 'index.html');
    const indexStat = await fs.stat(indexPath);
    if (indexStat.isFile()) {
      sendFile(indexPath, req, res, indexStat);
    } else {
      throw new Error('index.html missing!');
    }
  }

  /**
   * 下载远程文件然后返回
   * @author  韦胜健
   * @date    2024.3.2 22:59
   */
  function sendRemote(url: string, req: IncomingMessage, res: ServerResponse) {
    const fileType = mime.getType(url) || 'text/html';
    // console.log({ url, fileType });
    res.setHeader('Content-Type', fileType + ';charset=utf-8');
    if (config.cors) {res.setHeader('Access-Control-Allow-Origin', '*');}
    fetch(url).then((r: any) => {
      r.body.pipe(res);
    });
  }

  /**
   * 处理请求
   * @author  韦胜健
   * @date    2023/3/7 16:40
   */
  async function handleRequest(req: IncomingMessage, res: ServerResponse) {

    let { pathname } = url.parse(req.url || '');
    if (!pathname) {pathname = '/';}
    pathname = pathJoin(`/${pathname}`);
    // console.log({ pathname, isProxy });

    if (isProxy) {
      const url = pathJoin(config.dir, pathname);
      sendRemote(url, req, res);
    } else {
      if (!!config.base && !!config.base.length && pathname.startsWith(config.base)) {pathname = pathname.slice(config.base.length);}
      pathname = decodeURIComponent(pathname);

      let absPath = path.join(config.dir, pathname);

      try {
        const stat = await fs.stat(absPath);
        if (stat.isDirectory()) {
          const indexPath = path.join(absPath, 'index.html');
          const indexStat = await fs.stat(indexPath);
          if (indexStat.isFile()) {
            logDebug({ pathname, indexPath, });
            sendFile(indexPath, req, res, indexStat);
          } else {
            if (config.single) {
              logDebug({ pathname, msg: 'no match file, return index path', });
              await sendIndex(req, res);
            } else {
              throw new Error('no file match path:' + pathname);
            }
          }
        } else {
          logDebug({ pathname, });
          sendFile(absPath, req, res, stat);
        }
      } catch (e) {
        if (config.single) {
          logDebug({ pathname, msg: 'no match file, return index path', });
          await sendIndex(req, res);
        } else {
          sendError({ error: e, pathname }, res);
        }
      }
    }
  }

  function start() {
    return http.createServer(handleRequest).listen(config.port, () => {
        console.log(`
${chalk.yellow(`Starting up http-server, serving:`)}
${isProxy ? `代理远程地址${config.dir}` : `代理本地文件${config.dir}`} :
${pathJoin(`http://127.0.0.1:${chalk.green(config.port)}`, config.base)}
Hit Ctrl-C to stop the Server
            `.trim());
      }
    );
  }

  return { start };
}

export function pathJoin(...args: (string | null | undefined)[]): string {
  let val: string = args.filter(i => {
    if (i == null) {return false;}
    if (i.trim() == '/') {return false;}
    return true;
  }).reduce((prev: any, item: any) => {
    if (!prev) {return item;}
    return prev + '/' + item;
  }, '');

  const prependStringList = ['http://', 'https://'];

  const prependString = prependStringList.find(i => val.indexOf(i) === 0);

  let ret: string;
  if (!prependString) {
    /*非http.https开头*/
    ret = val.replace(/\/{2,}/g, '/');
  } else {
    const leftString = val.slice(prependString.length);
    // console.log({ prependString, leftString });
    ret = prependString + leftString.replace(/\/{2,}/g, '/');
  }

  if (ret === '/') {
    return '';
  }

  return ret;
}
