# Ferv

用来搭建一个简易的文件代理服务器，主要用来测试打包之后的前端资源；

## 安装

```bash
npm i ferv -g
```

或者

```bash
cnpm i ferv -g
```

或者

```bash
yarn add ferv -g
```

## 基本用法

- 在前端工程根目录下打包完毕之后，比如打包的结果目录为`dist`；
- 在工程根目录下执行 `ferv ./dist`
- 点击在控制台打印出来的访问路径，或者自行在浏览器中访问；

## 命令参数

### -h, --help

查看所有参数说明

### -p, --port

设置端口，示例`ferv ./dist -p 3301`

### -s, --single

单页面应用需要开启这个属性，当路由对应的资源不存在时返回 /index.html，示例`ferv ./dist -s`

### -b, --base

- 设置公共资源路径，也就是webpack中配置的publicPath；注意的是这个路径参数前面不能带/，否则会自动加上系统盘符前缀；
- 比如打包的时候设置的publicPath为`/xxx/`
- 执行命令的时候可以是`xxx`或者`xxx/`，示例`ferv ./dist -b xxx`

### -d, --debug

- 打印所有的请求信息
- 示例：`ferv ./dist -d`

### -c, --cors

- 响应头添加Access-Control-Allow-Origin:*用来跨域
- 示例：`ferv ./dist -c`
